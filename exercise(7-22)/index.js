let trainer = {
	name: "Ash Ketchum",
	age: "12",
	numberOfBadges: 6,
	friends: ["Misty","Gary","Mae"],
	pokemons: ["Pikachu","Squirtle","Charmander"],

	introduce: function(){
		console.log("Hi i'm " + trainer.name + ". I'm " + trainer.age + " years old. I have " + trainer.numberOfBadges + " badges");
	},

	catch: function(target){
		if(trainer['pokemons'].length === 6){
			console.log("A trainer should only have 6 pokemons to carry");
		} else {
			trainer.pokemons.push(target);
			console.log("Gotcha " + target + "!");
		}
	},

	release: function(){
		if(trainer['pokemons'].length === 0){
			alert("You have no more pokemons! Catch one first.");
		} else{
			trainer.pokemons.pop();
		}
	}
}


trainer.introduce();
trainer.catch("Bulbasaur");
console.log(trainer.pokemons);
trainer.release();
console.log(trainer.pokemons);
trainer.catch("Rayquaza");
trainer.catch("Groudon");
trainer.catch("Kyogre");
trainer.catch("Bulbasaur");
trainer.release();
trainer.release();
trainer.release();
trainer.release();
trainer.release();
trainer.release();

let courses = `
	[
		{
			"name": "Philosophy 101",
			"description": "Study Life",
			"price": 2400,
			"isActive": true
		},
		{
			"name": "Basic Human Anatomy 101",
			"description": "Study the human body",
			"price": 2300,
			"isActive": true
		},
		{
			"name": "History 101",
			"description": "Study history",
			"price": 1500,
			"isActive": true
		}
	]
`;

let parsedCourses = JSON.parse(courses);
parsedCourses.pop();
// console.log(parsedCourses);