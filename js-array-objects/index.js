//	ACTIVITY 1 - forEach()

/*
Use forEach() to display the usernames in the console.
Create a function called register() able to receive two strings as arguments.
    -push() a new user object into the users array with values from the parameter.
    -new object pushed should have a username and password.
To enable screen reader support, press Ctrl+Alt+Z To learn about keyboard shortcuts, press Ctrl+slash
*/
 

let users = [


        {
                username: "cliffBass",
                password: "metallibass80"
        },
        {
                username: "neryJeepie",
                password: "arturo1991"
        },


];

users.forEach((index) => {
	console.log(index);
})

function register(username,password){
	this.username = username,
	this.password = password
}

users.push(new register("userOne","safePassword"));
console.log(users);


//	ACTIVITY 2 - Switch/Case
/*
Create a function called displayDate which will be able to show the date today as an alert

Create a function called displayDayAndColor which will be able to get the current day and show the color of the day
	-Create a new variable to save the current day today.
	-Create a switch which will be able to show an alert message about the color of the day based on the day today.
	-This function will not receive arguments or return data.
*/

/*
var.toDateString(); method converts a date to a more readable format

Days: 0-6 (Sun-Sat)
*/

let fullDate = new Date();

function displayDate(){
	alert(fullDate.toDateString());
}
displayDate();

let day = new Date().getDay();


switch(day){
	case 0:
		console.log("Sunday");
		console.log("The color of the day is Pink");
	break;
	case 1:
		console.log("Monday");
		console.log("The color of the day is Red");
	break;
	case 2:
		console.log("Tuesday");
		console.log("The color of the day is Blue");
	break;
	case 3:
		console.log("Wednesday");
		console.log("The color of the day is Purple");
	break;
	case 4:
		console.log("Thursday");
		console.log("The color of the day is Orange");
	break;
	case 5:
		console.log("Friday");
		console.log("The color of the day is Yellow");
	break;
	case 6:
		console.log("Saturday");
		console.log("The color of the day is Black");
	break;
	default:
		console.log("cannot generate colors at the moment");
	break;
}





//	ACTIVITY 3 - for()
/*
	Create an odd-even checker that will check which numbers from 1-300 are odd and which are even,

	Sample output in the console: 
	1 - odd
	2 - even
	3 - odd
	4 - even
	5 - odd
	etc. 
*/

/* FOR!!!!!
1. the declaration of  counter
2, the condition that will be evaluated to determine if the loop will continue
3. the iteration or the incrementation/decrementation need to continue and arrive at a terminating/end condition
// */

for(let number = 1; number <= 300; number++){
	if(number % 2 === 0){
		console.log(number + " - even");
	} else{
		console.log(number + " - odd");
	}
}


